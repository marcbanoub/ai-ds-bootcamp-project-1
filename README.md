# AI-DS-BootCamp-Project-1

Bootcamp bonus project

**Challenge**

The challenge is to push yourself after the bootcamp, and show-off your skills in AI and its subfields, 
specifically in 3 sub-fields, NLP, Time-series analysis, and Computer Vision.
you'll be provided with a dataset for each field,  
and your goal to produce the best model and the best application using this dataset.  
the best model will be in terms of innovation, accuracy and speed.

**How to  submit**
Open a merge request with your code

Gitlab and Git tutorial : https://gist.github.com/m-kyle/fb0f3e9edc369adfcac7

Alternative tutorial : https://indico.him.uni-mainz.de/event/37/material/slides/0.pdf



**What to submit**

Jupyter Notebook with your work
Python code for training and testing
Model in pkl or h5 format
Instructions for how to run it


**Deadline**
29 Jan 2020 , 11:59 PM - No exceptions.

Datasets

**Time-Series**
https://www.kaggle.com/c/restaurant-revenue-prediction
Right now, deciding when and where to open new restaurants is largely a subjective process based on the personal judgement and experience of development teams. 
This subjective data is difficult to accurately extrapolate across geographies and cultures.
New restaurant sites take large investments of time and capital to get up and running. 
When the wrong location for a restaurant brand is chosen, the site closes within 18 months and operating losses are incurred.
Finding a mathematical model to increase the effectiveness of investments in new restaurant sites would allow 
to invest more in other important business areas, like sustainability, innovation, and training for new employees.
Using demographic, real estate, and commercial data, this competition challenges you to predict the annual restaurant sales of many regional locations.

**Computer Vision**
https://www.kaggle.com/kmader/food41
The dataset contains a number of different subsets of the full food-101 data.  
The idea is to make a more exciting simple training set for image analysis than CIFAR10 or MNIST. 
For this reason the data includes massively downscaled versions of the images to enable quick tests.  
The data has been reformatted as HDF5 and specifically Keras HDF5Matrix which allows them to be easily read in.  
The file names indicate the contents of the file. For example food_c101_n1000_r384x384x3.h5 means there are 101 categories represented, with n=1000 images,  
that have a resolution of 384x384x3 (RGB, uint8) 
food_test_c101_n1000_r32x32x1.h5 means the data is part of the validation set,  
has 101 categories represented, with n=1000 images, that have a resolution of 32x32x1 (float32 from -1 to 1)
The first goal is to be able to automatically classify an unknown image using the dataset,  
but beyond this there are a number of possibilities for looking at what regions / image components are important for making classifications,  
identify new types of food as combinations of existing tags, build object detectors which can find similar objects in a full scene. 

**NLP**
https://www.kaggle.com/c/restaurant-reviews#
The goal of the dataset is to learn which reviews are negative and which are positive, and evaluate new restaurant reviews based on learning features 
from the dataset, possibly extended to other reviews not only restaurants and food
